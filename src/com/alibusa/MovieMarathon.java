package com.alibusa;

import java.util.*;

public class MovieMarathon {
    private boolean exit;
    private int option;
    private String name, category;
    ArrayList<Movie> movieList = new ArrayList<>();

    public void runStore(){
        welcome();
        menu();
    }

    private void welcome() {
        System.out.println("+==================================+");
        System.out.println("|    Welcome to Movie Marathon!    |");
        System.out.println("+==================================+");
    }

    private void menu() {
        while (!exit) {
            selection();
            option = choiceInput();
            cases(option);
        }
    }

    private void selection() {
        System.out.println("Please make a selection:");
        System.out.println("1. Add Movie");
        System.out.println("2. View All Movies");
        System.out.println("3. Exit");
    }

    private int choiceInput() {
        Scanner optionInput = new Scanner(System.in);
        option = 0;
            while (option <= 0 || option > 3) {
                try {
                    if(option < 0 || option > 3) {
                        System.out.println("*** Invalid input. Please try again. ***");
                    }
                    System.out.print("Enter choice number: ");
                    option = Integer.parseInt(optionInput.nextLine());
                }
                catch (NumberFormatException e) {
                    System.out.println("*** Invalid input. Please try again. ***");
                }
            }
        return option;
    }

    private void cases(int option) {
        switch(option){
            case 1:
                addMovie();
                break;
            case 2:
                displayMovies();
                break;
            case 3:
                exit = true;
                System.out.println("Exiting... Thank you for using Movie Marathon! Goodbye!");
                break;
        }
    }

    private void addMovie() {
        Scanner nameInput = new Scanner(System.in);
        Scanner categoryInput = new Scanner(System.in);

        System.out.print("Please enter the movie title: ");
        name = nameInput.nextLine().trim();
        while (name.isEmpty() || name.equals(" ")) {
            System.out.println("*** Invalid input. Please try again. ***");
            System.out.print("Please enter the movie title: ");
            name = nameInput.nextLine().trim();
        }

        System.out.print("Please enter the movie category: ");
        category = categoryInput.nextLine().trim();
        while (category.isEmpty() || category.equals(" ")) {
            System.out.println("*** Invalid input. Please try again. ***");
            System.out.print("Please enter the movie category: ");
            category = categoryInput.nextLine().trim();
        }

        Movie addMovie = new Movie(name, category);
        movieList.add(addMovie);

        System.out.println("*** " + name + " has been successfully added. ***");
    }

    private void displayMovies() {
        if(movieList.size() == 0) {
            System.out.println("*** No available movie. ***");

        } else {
            System.out.println("*** Available Movies ***");
            for (int i = 0; i < movieList.size(); i++){
                System.out.println((i+1) + ". " + movieList.get(i).display());
            }
            System.out.println("***   End of List   ****");
        }
    }
}
