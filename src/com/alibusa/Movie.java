package com.alibusa;

public class Movie {

    private String name, category;

    public Movie(String newName, String newCategory) {
        this.name = newName;
        this.category = newCategory;
    }

    // Getters
    public String getName() {
        return this.name;
    }
    public String getCategory() {
        return this.category;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }
    public void setCategory(String newCategory) {
        this.category = newCategory;
    }

    // Display
    public String display() {
        return this.getName() + "; " + this.getCategory();
    }
}
